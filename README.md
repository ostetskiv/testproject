using System;
using System.Net.Http;

namespace ConsoleApp2
{
    public class Program
    {
        static HttpClient client = new HttpClient();
        static void Main(string[] args)
        {
            Program program = new Program();
            string s =  GetJson("https://gitlab.com/api/v4/projects/24865550/repository/files/README.md/raw?ref=master");
            Console.WriteLine(s);
        }

        static string GetJson(string path)
        {
            string jsonDocument = null;
            HttpResponseMessage response =  client.GetAsync(path).Result;
            
            jsonDocument = response.Content.ReadAsStringAsync().Result;
            return jsonDocument;
        }
    }
}

